# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
    random = Random.new
    num = random.rand(1...100)
    guess = 0
    @num_of_guesses = 1
    until (1..99).include?(guess)
      puts "Guess a Number"
      guess = gets.chomp.to_i
      guess_range(guess, num)
     @num_of_guesses += 1   
    end
    until guess == num
      puts "try again"
      guess = gets.chomp.to_i
      guess_range(guess, num)
      @num_of_guesses += 1
    end
end

def guess_range(guess, random)
  if guess > 99
    puts "#{guess} is too high please pick between 1 and 100!"
  elsif guess < 1
    puts "#{guess} is too low please pick between 1 and 100!"
  elsif guess > random
    puts "Your guess of #{guess} is too high ! Guess ##{@num_of_guesses}"
  elsif guess < random
    puts "Your guess of #{guess} is too low ! Guess ##{@num_of_guesses}"
  else
    puts "Your guess of #{guess} is correct!!! You Win!!!!! Only #{@num_of_guesses} guesses!"
  end
end

def file_shuffler
  puts "Specify a file directory to be shuffled:"
  file_directory = gets.chomp
  arr = File.readline(file_directory)
  arr.shuffle
end